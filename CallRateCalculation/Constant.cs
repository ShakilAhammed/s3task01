﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CallRateCalculation
{
    public class Constant
    {


        public static TimeSpan PeakTimeStart = new TimeSpan(9, 00, 00);
        public static TimeSpan PeakTimeEnd = new TimeSpan(22, 59, 00);
        public static TimeSpan OffPeakTimeStart = new TimeSpan(0, 00, 00);
        public static TimeSpan OffPeakTimeEnd = new TimeSpan(8, 59, 59);
        public static TimeSpan OptionalOffPeakTimeStart = new TimeSpan(23, 00, 00);
        public static TimeSpan OptionalOffPeakTimeEnd = new TimeSpan(23, 59, 59);
    }
}
