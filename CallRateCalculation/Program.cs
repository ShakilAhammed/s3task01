﻿using System;

namespace CallRateCalculation
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Start and End Time : ");
            DateTime startTime;
            DateTime endTime;
            if (DateTime.TryParse(Console.ReadLine(), out startTime) && DateTime.TryParse(Console.ReadLine(), out endTime))
            {
                Console.WriteLine($"you have charged {startTime.CalculateCharge(endTime)} tk");
            }
            else
            {
                Console.WriteLine("You have entered incorrect value.");
            }

            Console.ReadKey();
        }
    }

    public static class Extensions
    {
        public static double CalculateCharge(this DateTime startDate, DateTime endDate)
        {
            double poisa = 0.0;
            while (startDate <= endDate)
            {
                startDate = startDate.AddSeconds(21);
                poisa += startDate.TimeOfDay.GetPulseRate();
            }
            return poisa/100;
        }

        public static double GetPulseRate(this TimeSpan timeSpan)
        {
            if ((timeSpan >= Constant.OffPeakTimeStart && timeSpan <= Constant.OffPeakTimeEnd) || (timeSpan >= Constant.OptionalOffPeakTimeStart && timeSpan <= Constant.OptionalOffPeakTimeEnd))
                return 20.0;
            return 30.0;
        }
    }
}
